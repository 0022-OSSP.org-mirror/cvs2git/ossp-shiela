   _        ___  ____ ____  ____        _     _      _
  |_|_ _   / _ \/ ___/ ___||  _ \   ___| |__ (_) ___| | __ _
  _|_||_| | | | \___ \___ \| |_) | / __| '_ \| |/ _ \ |/ _` |
 |_||_|_| | |_| |___) |__) |  __/  \__ \ | | | |  __/ | (_| |
  |_|_|_|  \___/|____/____/|_|     |___/_| |_|_|\___|_|\__,_|

  OSSP shiela - Access Control and Logging Facility for CVS
  Version

  ABSTRACT

  OSSP shiela is an access control and logging facility for use with the
  Concurrent Versions System (CVS). It is intended to be hooked into
  CVS's processing through the $CVSROOT/CVSROOT/xxxinfo callbacks. This
  way OSSP shiela provides access control on a path and branch basis to
  particular repository users and user groups. Additionally, repository
  operations are monitored, accumulated and logged. The lookout of
  logging messages can be configured individually on a module path and
  branch basis and messages can be both saved to files and/or delivered
  by email.

  COPYRIGHT AND LICENSE

  Copyright (c) 2000-2006 Ralf S. Engelschall <rse@engelschall.com>
  Copyright (c) 2000-2006 The OSSP Project <http://www.ossp.org/>

  This file is part of OSSP shiela, an access control and logging
  facility for Concurrent Versions System (CVS) repositories
  which can be found at http://www.ossp.org/pkg/tool/shiela/.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.

  SEE ALSO

  o Homepage ....... http://www.ossp.org/pkg/tool/shiela/
  o Sources ........ http://cvs.ossp.org/pkg/tool/shiela/
  o Distribution .... ftp://ftp.ossp.org/pkg/tool/shiela/

