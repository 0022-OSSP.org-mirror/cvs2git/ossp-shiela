#!/bin/sh
##
##  OSSP shiela - CVS Access Control and Logging Facility
##  Copyright (c) 2000-2006 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2000-2006 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP shiela, an access control and logging
##  facility for Concurrent Versions System (CVS) repositories
##  which can be found at http://www.ossp.org/pkg/tool/shiela/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  shiela-test.sh: simple test suite (syntax: Bourne-Shell)
##

#   determine absolute path
abspath () {
    cwd=`pwd`
    echo "$1" |\
    sed -e "s;^\\.\$;$cwd;" \
        -e "s;^\\./;$cwd/;" \
        -e "s;^\\([^/]\\);$cwd/\\1;"
}

#   configuration
if [ ".$1" != . ]; then
    path_cvs=`abspath $1`
else
    path_cvs=`which cvs`
fi
if [ ! -f $path_cvs ]; then
	echo "shiela-test:ERROR: invalid path to CVS program: $path_cvs"
    exit 1
fi
if [ ".$2" != . ]; then
    path_perl=`abspath $2`
else
    path_perl=`which perl`
fi
if [ ! -f $path_perl ]; then
	echo "shiela-test:ERROR: invalid path to Perl program: $path_perl"
    exit 1
fi
if [ ".$3" != . ]; then
    path_shtool=`abspath $3`
else
    path_shtool=`which shtool`
fi
if [ ! -f $path_shtool ]; then
	echo "shiela-test:ERROR: invalid path to Shtool program: $path_shtool"
    exit 1
fi
if [ ".$4" != . ]; then
    path_shiela=`abspath $4`
else
    path_shiela=`which shiela`
fi
if [ ! -f $path_shiela ]; then
	echo "shiela-test:ERROR: invalid path to Shiela program: $path_shiela"
    exit 1
fi
if [ ".$5" != . ]; then
    path_shiela_install=`abspath $5`
else
    path_shiela_install=`which shiela-install`
fi
if [ ! -f $path_shiela_install ]; then
	echo "shiela-test:ERROR: invalid path to Shiela Install program: $path_shiela_install"
    exit 1
fi
if [ ".$6" != . ]; then
	cwd=`pwd`
    path_test=`echo "$6" | sed -e "s;^\\.\$;$cwd;" -e "s;^\\./;$cwd/;" -e "s;^\\([^/]\\);$cwd/\\1;"`
else
    path_test="`pwd`/shiela-test.d"
fi
if [ ".$path_test" = . ]; then
	echo "shiela-test:ERROR: empty path to test tree"
    exit 1
fi
if [ ".$7" != . ]; then
	cwd=`pwd`
    path_log=`echo "$7" | sed -e "s;^\\.\$;$cwd;" -e "s;^\\./;$cwd/;" -e "s;^\\([^/]\\);$cwd/\\1;"`
else
    path_log="`pwd`/shiela-test.log"
fi
if [ ".$path_log" = . ]; then
	echo "shiela-test:ERROR: empty path to logfile"
    exit 1
fi

#   print header 
header () {
	msg="$1"
    echo "" 1>&2
    echo "#######################################################################" 1>&2
	$path_shtool echo -e "##  %B$msg%b" 1>&2
    echo "#######################################################################" 1>&2
    echo "" 1>&2
}

#   prepare test directory
oldcwd=`pwd`
rm -rf $path_test >/dev/null 2>&1 || true
mkdir $path_test || exit 1
cd $path_test || exit 1

#   prepare sendmail emulator for logfile writing
cp /dev/null $path_log
( echo "#!/bin/sh"
  echo "cat >>$path_log"
  echo "exit 0"
) >sendmail
chmod a+x sendmail
path_sendmail=`abspath sendmail`

#   create new repository 
rm -rf cvs
mkdir cvs
cvs -d $path_test/cvs init

#   setup CVSUSER variable
export CVSUSER=jdoe

#   apply shiela to repository
header "Apply Shiela to CVS Repository"
$path_shiela_install \
    batch=yes \
    repos_path=$path_test/cvs \
    tool_shiela=$path_shiela \
    tool_cvs=$path_cvs \
    tool_perl=$path_perl \
    tool_sendmail=$path_sendmail \
    shiela_local=no \
    host_name="cvs" \
    domain_name="example.com" \
    user_name="jdoe" \
    user_realname="John Doe" \
    user_email="john.doe@example.com"

#   activate forthcoming module in shiela
header "Activate Forthcoming Module in Shiela"
rm -rf CVSROOT
cvs -d :fork:$path_test/cvs co CVSROOT
patch -p0 <<EOT
--- CVSROOT/shiela.cfg.orig	Wed May  5 20:02:24 2004
+++ CVSROOT/shiela.cfg	Wed May  5 20:02:45 2004
@@ -61,6 +61,12 @@
             Log passwd none;
             Log * mail:john.doe@example.com;
         };
+        Module foo "The Foo Module" {
+            Acl *   *:users;
+            Acl *:* *:users;
+            Log *   mail:john.doe@example.com;
+            Log *:* mail:john.doe@example.com;
+        };
     };
 };
EOT
(cd CVSROOT && cvs ci -m "activate foo module" shiela.cfg)
 
#   add module 'foo' to repository by importing
header "Add Module to CVS Repository (cvs import)"
rm -rf foo
mkdir foo
echo "bar" >foo/bar.txt
echo "baz" >foo/baz.txt
(cd foo && cvs -d :fork:$path_test/cvs import -m "initial content" foo X Y)
rm -rf foo

#   checking out module 'foo'
header "Checking Out Module from CVS Repository"
cvs -f -d :fork:$path_test/cvs co foo

#   edit module 'foo' by committing
header "Edit Module in CVS Repository (cvs commit)"
( cd foo || exit 1
  ( echo "bar1"
    echo "bar"
    echo "bar2"
  ) >bar.txt
  rm baz.txt
  cvs rm baz.txt
  echo "quux" >quux.txt
  cvs add quux.txt
  ( echo "many modifications"
    echo "PR:"
  ) >log
  cvs ci -F log
  rm -f log
)

#   tag module files
header "Tagging Module Files in CVS Repository (cvs tag)"
( cd foo || exit 1
  cvs tag BAR_1_0 bar.txt
  cvs tag BAR_1_1 bar.txt
  cvs tag BAR_1_2 bar.txt
)

#   binary file handling
header "Handling Binary Files in CVS Repository"
( cd foo || exit 1
  echo "quux" >quux.bin
  cvs add -kb quux.bin
  cvs ci -m "add binary file"
  echo "quux2" >quux.bin
  cvs ci -m "change binary file"
  rm quux.bin
  cvs rm quux.bin
  cvs ci -m "remove binary file"
)

#   cleanup
cd $oldpwd
rm -rf $path_test >/dev/null 2>&1 || true

