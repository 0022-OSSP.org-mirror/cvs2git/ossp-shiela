#!@SH@
##
##  OSSP shiela - CVS Access Control and Logging Facility
##  Copyright (c) 2000-2006 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2000-2006 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP shiela, an access control and logging
##  facility for Concurrent Versions System (CVS) repositories
##  which can be found at http://www.ossp.org/pkg/tool/shiela/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  shiela-install.sh: repository install program (syntax: Bourne-Shell)
##

version="1.1.7"

prefix="@prefix@"
bindir="@bindir@"

if [ ".$TMPDIR" != . ]; then
    tmpdir=$TMPDIR
elif [ ".$TEMPDIR" != . ]; then
    tmpdir=$TEMPDIR
else
    tmpdir=/tmp
fi
tmpfile="$tmpdir/shiela-install.$$.tmp"

#   determine terminal bold sequence
term_bold='' 
term_norm=''
case $TERM in
    #   for the most important terminal types we directly know the sequences
    xterm|xterm*|vt220|vt220*)
        term_bold=`awk 'BEGIN { printf("%c%c%c%c", 27, 91, 49, 109); }' </dev/null 2>/dev/null`
        term_norm=`awk 'BEGIN { printf("%c%c%c", 27, 91, 109); }' </dev/null 2>/dev/null`
        ;;
    vt100|vt100*)
        term_bold=`awk 'BEGIN { printf("%c%c%c%c%c%c", 27, 91, 49, 109, 0, 0); }' </dev/null 2>/dev/null`
        term_norm=`awk 'BEGIN { printf("%c%c%c%c%c", 27, 91, 109, 0, 0); }' </dev/null 2>/dev/null`
        ;;
    #   for all others, we try to use a possibly existing `tput' or `tcout' utility
    * )
        paths=`echo $PATH | sed -e 's/:/ /g'`
        for tool in tput tcout; do
            for dir in $paths; do
                if [ -r "$dir/$tool" ]; then
                    for seq in bold md smso; do # 'smso' is last
                        bold="`$dir/$tool $seq 2>/dev/null`"
                        if [ ".$bold" != . ]; then
                            term_bold="$bold"
                            break
                        fi
                    done
                    if [ ".$term_bold" != . ]; then
                        for seq in sgr0 me rmso reset; do # 'reset' is last
                            norm="`$dir/$tool $seq 2>/dev/null`"
                            if [ ".$norm" != . ]; then
                                term_norm="$norm"
                                break
                            fi
                        done
                    fi
                    break
                fi
            done
            if [ ".$term_bold" != . -a ".$term_norm" != . ]; then
                break;
            fi
        done
        ;;
esac

echo "This is ${term_bold}OSSP shiela Setup${term_norm}, Version ${term_bold}${version}${term_norm}"
echo "Copyright (c) 2000-2002 Ralf S. Engelschall <rse@engelschall.com>"
echo "Copyright (c) 2000-2002 The OSSP Project <http://www.ossp.org/>"
echo ""

echo "${term_bold}PARAMETER DETERMINATION${term_norm}"
cat <<EOT

This utility helps you to initially setup OSSP shiela for a particular CVS
repository. It asks you a few questions in order to establish an initial
OSSP shiela configuration. This can be adjusted later without problems. Only
the questions marked with (U) ask for details which are actually used
immediately and so have to be correctly entered already now. All other
questions ask just for informational (I) details which do not harm if
not immediately correct.

EOT

for arg
do
    eval `echo "$arg" | sed -e 's/^\([^=]*\)=\(.*\)$/V_\1="\2"/'`
done

query () {
    vartype=$1
    varname=$2
    echo $varname | awk '{ printf("%-16s", $0); }'
    eval "text=\"($vartype) [\$V_$varname]: \""
    echo dummy | awk '{ printf("%s", TEXT); }' TEXT="$text"
    if [ ".$V_batch" = .yes ]; then
        echo ""
    else
        read answer
        if [ ".$answer" != . ]; then
            eval "V_${varname}=\"\$answer\""
        fi
    fi
}

domainname=''
hostname="`(uname -n) 2>/dev/null |\
           awk '{ printf("%s", $1); }'`"
if [ ".$hostname" = . ]; then
    hostname="`(hostname) 2>/dev/null |\
               awk '{ printf("%s", $1); }'`"
    if [ ".$hostname" = . ]; then
        hostname='unknown'
    fi
fi
case $hostname in
    *.* )
        domainname="`echo $hostname | cut -d. -f2-`"
        hostname="`echo $hostname | cut -d. -f1`"
        ;;
esac
if [ ".$domainname" = . ]; then
    if [ -f /etc/resolv.conf ]; then
        domainname="`egrep '^[ 	]*domain' /etc/resolv.conf | head -1 |\
                     sed -e 's/.*domain//' \
                         -e 's/^[ 	]*//' -e 's/^ *//' -e 's/^	*//' \
                         -e 's/^\.//' |\
                     awk '{ printf("%s", $1); }'`"
        if [ ".$domainname" = . ]; then
            domainname="`egrep '^[ 	]*search' /etc/resolv.conf | head -1 |\
                         sed -e 's/.*search//' \
                             -e 's/^[ 	]*//' -e 's/^ *//' -e 's/^	*//' \
                             -e 's/ .*//' -e 's/	.*//' \
                             -e 's/^\.//' |\
                         awk '{ printf("%s", $1); }'`"
        fi
    fi
fi
if [ ".$V_host_name" = . ]; then
    V_host_name=$hostname
fi
query I host_name
if [ ".$V_domain_name" = . ]; then
    V_domain_name=$domainname
fi
query U domain_name

if [ ".$V_user_name" = . ]; then
    V_user_name="$LOGNAME"
    if [ ".$V_user_name" = . ]; then
        V_user_name="$USER"
        if [ ".$V_user_name" = . ]; then
            V_user_name="`(whoami) 2>/dev/null |\
                          awk '{ printf("%s", $1); }'`"
            if [ ".$V_user_name" = . ]; then
                V_user_name="`(who am i) 2>/dev/null |\
                              awk '{ printf("%s", $1); }'`"
                if [ ".$V_user_name" = . ]; then
                    V_user_name='unknown'
                fi
            fi
        fi
    fi
fi
query U user_name

if [ ".$V_user_realname" = . ]; then
    V_user_realname=`(cat /etc/passwd; (ypcat passwd) 2>/dev/null) |\
                     egrep "^$V_user_name:" 2>/dev/null | \
                     sed -e 's/[^:]*:[^:]*:[^:]*:[^:]*://' -e 's/:.*$//'`
fi
query I user_realname

if [ ".$V_user_email" = . ]; then
    V_user_email="${V_user_name}@${V_domain_name}"
fi
query U user_email

if [ ".$V_project_tag" = . ]; then
    V_project_tag="`echo ${V_user_name} |\
                    tr 'abcdefghijklmnopqrstuvwxyz' 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'`"
fi
query I project_tag

if [ ".$V_project_name" = . ]; then
    V_project_name="${V_user_realname}'s CVS Test Project"
fi
query I project_name

if [ ".$V_project_home" = . ]; then
    V_project_home="http://${V_host_name}.${V_domain_name}/~${V_user_name}/"
fi
query I project_home

if [ ".$V_project_contact" = . ]; then
    V_project_contact=${V_user_email}
fi
query I project_contact

if [ ".$V_repos_tag" = . ]; then
    V_repos_tag="CVS-`echo ${V_user_name} |\
                 tr 'abcdefghijklmnopqrstuvwxyz' 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'`"
fi
query I repos_tag

if [ ".$V_repos_name" = . ]; then
    V_repos_name="${V_user_realname}'s CVS Test Repository"
fi
query I repos_name

if [ ".$V_repos_contact" = . ]; then
    V_repos_contact=${V_project_contact}
fi
query I repos_contact

if [ ".$V_repos_home" = . ]; then
    V_repos_home=${V_project_home}
fi
query I repos_home

if [ ".$V_repos_host" = . ]; then
    V_repos_host="${V_host_name}.${V_domain_name}"
fi
query U repos_host

if [ ".$V_repos_path" = . ]; then
    V_repos_path="${CVSROOT-/tmp}"
fi
query U repos_path
if [ ! -d "$V_repos_path/CVSROOT" ]; then
    echo "shiela-install:Error: \`$V_repos_path' does not look like a CVS repository" 1>&2
    exit 1
fi

if [ ".$V_repos_history" = . ]; then
    V_repos_history="CVSROOT/shiela.log"
fi
query U repos_history

if [ ".$V_env_path" = . ]; then
    V_env_path="/bin:/usr/bin:/sbin:/usr/sbin"
fi
query I env_path

#   check whether the test command supports the -x option
if [ -x /bin/sh ] 2>/dev/null; then
    minusx="-x"
else
    minusx="-r"
fi
#   split path string
paths="`echo $PATH |\
        sed -e 's/^:/.:/' \
            -e 's/::/:.:/g' \
            -e 's/:$/:./' \
            -e 's/:/ /g'`"

if [ ".$V_tool_perl" = . ]; then
    V_tool_perl="perl"
    rm -f $tmpfile
    touch $tmpfile
    found=0
    pc=99
    for dir in $paths; do
        dir=`echo $dir | sed -e 's;/*$;;'`
        nc=99
        for name in perl perl5 miniperl; do
             if [ $minusx "$dir/$name" -a ! -d "$dir/$name" ]; then
                 perl="$dir/$name"
                 pv=`$perl -e 'printf("%.3f", $]);'`
                 echo "$pv:$pc:$nc:$perl" >>$tmpfile
                 found=1
             fi
             nc=`expr $nc - 1`
        done
        pc=`expr $pc - 1`
    done
    if [ $found = 1 ]; then
        V_tool_perl="`cat $tmpfile | sort -u | tail -1 | cut -d: -f4`"
    fi
    rm -f $tmpfile
fi
query U tool_perl

if [ ".$V_tool_sendmail" = . ]; then
    V_tool_sendmail="sendmail"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/sendmail" ] && [ ! -d "$path/sendmail" ]; then
            V_tool_sendmail="$path/sendmail"
            break
        fi
    done
fi
query I tool_sendmail

if [ ".$V_tool_diff" = . ]; then
    V_tool_diff="diff"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/diff" ] && [ ! -d "$path/diff" ]; then
            V_tool_diff="$path/diff"
            break
        fi
    done
fi
query I tool_diff

if [ ".$V_tool_xdelta" = . ]; then
    V_tool_xdelta="xdelta"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/xdelta" ] && [ ! -d "$path/xdelta" ]; then
            V_tool_xdelta="$path/xdelta"
            break
        fi
    done
fi
query I tool_xdelta

if [ ".$V_tool_uuencode" = . ]; then
    V_tool_uuencode="uuencode"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/uuencode" ] && [ ! -d "$path/uuencode" ]; then
            V_tool_uuencode="$path/uuencode"
            break
        fi
    done
fi
query I tool_uuencode

if [ ".$V_tool_cvs" = . ]; then
    V_tool_cvs="cvs"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/cvs" ] && [ ! -d "$path/cvs" ]; then
            V_tool_cvs="$path/cvs"
            break
        fi
    done
fi
query U tool_cvs

CVS_VERSION=`$V_tool_cvs --version |\
             grep "Concurrent Versions System (CVS)" |\
             sed -e 's;^.*(CVS) ;;' -e 's; .*$;;'`
if [ ".`$V_tool_cvs --version | grep RSE`" != . ]; then
    CVS_VENDOR=RSE
else
    CVS_VENDOR=CVSHome.org
fi
case $CVS_VERSION in
   1.12.[6-9] | 1.12.[1-9][0-9] | 1.1[3-9].* )
       ;;
   * )
       echo "ERROR: CVS has to be version 1.12.6 or higher"
       exit 1
       ;;
esac
case $CVS_VENDOR in
    RSE )
       ;;
    * )
       echo "WARNING: You are using a stock CVS version from $CVS_VENDOR!"
       echo "         This means that not all OSSP shiela functionality is available"
       echo "         for you. We recommend to use the OpenPKG CVS version with"
       echo "         the enhancement patchset from Ralf S. Engelschall applied."
       echo "         [see http://www.openpkg.org/ for OpenPKG]"
       echo ""
       ;;
esac

if [ ".$V_tool_shiela" = . ]; then
    V_tool_shiela="$bindir/shiela"
    for path in $paths; do
        path=`echo $path | sed -e 's;/*$;;'`
        if [ $minusx "$path/shiela" ] && [ ! -d "$path/shiela" ]; then
            V_tool_shiela="$path/shiela"
            break
        fi
    done
fi
query U tool_shiela

if [ ".$V_shiela_local" = . ]; then
    V_shiela_local=yes
fi
query U shiela_local

cat >$tmpdir/shiela.cfg <<EOT
##
##  OSSP shiela - Access Control and Logging Facility for CVS
##  Copyright (c) 2000-2005 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2000-2005 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP shiela, an access control and logging
##  facility for Concurrent Versions System (CVS) repositories
##  which can be found at http://www.ossp.org/pkg/tool/shiela/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  shiela.cfg: run-time configuration (syntax: OSSP shiela C-style)
##

Environment {
    Setenv  PATH     ${V_env_path};
    Program sendmail ${V_tool_sendmail};
    Program cvs      ${V_tool_cvs};
    Program diff     ${V_tool_diff};
    Program xdelta   ${V_tool_xdelta};
    Program uuencode ${V_tool_uuencode};
};

Project {
    Tag     ${V_project_tag};
    Name    "${V_project_name}";
    Contact ${V_project_contact};
    Home    ${V_project_home};
    Users {
        User ${V_user_name} "${V_user_realname}" ${V_user_email};
    };
    Groups {
        Group users "Repository Users" { ${V_user_name} };
    };
};

Repository {
    Tag     ${V_repos_tag};
    Name    "${V_repos_name}";
    Contact ${V_repos_contact};
    Home    ${V_repos_home};
    Host    ${V_repos_host};
    Path    ${V_repos_path};
    History ${V_repos_history};
    Modules {
        Module CVSROOT "CVS Administrative Files" {
            Acl * *:users;
            Log passwd none;
            Log * mail:${V_user_email};
        };
    };
};

Logging {
    Reports {
        Report mail {
            Content title rule header files log summary rule details;
            Details patch:plain;
        };
    };
};

EOT

cat >$tmpdir/shiela.msg <<EOT

PR:
Submitted by:
Reviewed by:
Approved by:
Obtained from:
CVS: ----------------------------------------------------------------------
CVS: PR:            Fill this in if a problem report is affected
CVS: Submitted by:  Fill this in if someone else sent you the change
CVS: Reviewed by:   Fill this in if someone else reviewed your modification
CVS: Approved by:   Fill this in if you needed approval for this commit
CVS: Obtained from: Fill this in if the change is from third party software
CVS: ----------------------------------------------------------------------
EOT

echo ""
echo "${term_bold}SHIELA CONFIGURATION${term_norm}"
cat <<EOT

Initial "shiela.cfg" and "shiela.msg" files were generated for you.
Before OSSP shiela is installed and activated with these configuration files,
you have the chance to adjust them in your editor.

EOT
text="Edit \"shiela.cfg\" now with \"${EDITOR-vi}\" [y/N]: "
echo dummy | awk '{ printf("%s", TEXT); }' TEXT="$text"
if [ ".$V_batch" = .yes ]; then
    echo ""
else
    read answer
    if [ ".$answer" = .y ]; then
        cd $tmpdir && ${EDITOR-vi} shiela.cfg
    fi
fi
text="Edit \"shiela.msg\" now with \"${EDITOR-vi}\" [y/N]: "
echo dummy | awk '{ printf("%s", TEXT); }' TEXT="$text"
if [ ".$V_batch" = .yes ]; then
    echo ""
else
    read answer
    if [ ".$answer" = .y ]; then
        cd $tmpdir && ${EDITOR-vi} shiela.msg
    fi
fi
echo ""

echo "${term_bold}CVSROOT MODIFICATION${term_norm}"
cat <<EOT

Ok, we are now prepared to modify your repository. We will first
checkout CVSROOT and modify it. Then you have a last chance to stop
processing before we finally commit the modifications.

EOT

run () {
    echo "\$ $*" 1>&2
    eval $* || exit 1
}

cvs_file () {
    op=$1
    file=$2;
    name=`echo $file | sed -e 's;\.;_;g'`
    if [ ".$op" = .tst ]; then
        if [ -f $file ]; then
            eval "cvs_file_add_${name}=no"
        else
            eval "cvs_file_add_${name}=yes"
        fi
    elif [ ".$op" = .add ]; then
        eval "add=\$cvs_file_add_${name}"
        if [ ".$add" = .yes ]; then
            run $V_tool_cvs -q add $file
        fi
    fi
}

run rm -rf CVSROOT
run cvs -Q -q -d${V_repos_path} co CVSROOT
run cd CVSROOT

if [ ".$V_shiela_local" = .yes ]; then
    cvs_file tst shiela
    run "cp $V_tool_shiela ."
    run chmod 755 shiela
    cvs_file add shiela
    V_tool_shiela="\$CVSROOT/CVSROOT/shiela"
fi

cvs_file tst shiela.cfg
run cp $tmpdir/shiela.cfg .
run chmod 644 shiela.cfg
cvs_file add shiela.cfg

cvs_file tst shiela.msg
run cp $tmpdir/shiela.msg .
run chmod 644 shiela.msg
cvs_file add shiela.msg

cvs_file tst config
if [ ".`grep -i UseNewInfoFmtStrings config`" != . ]; then
    run "cp config config.old && sed -e 's;UseNewInfoFmtStrings=.*;UseNewInfoFmtStrings=yes;' <config.old >config && rm -f config.old"
else
    run "(echo ''; echo 'UseNewInfoFmtStrings=yes'; echo '') >>config"
fi
cvs_file add config

cvs_file tst checkoutlist
run "echo 'shiela.cfg' >>checkoutlist"
run "echo 'shiela' >>checkoutlist"
run "echo 'shiela.msg' >>checkoutlist"
cvs_file add checkoutlist

cvs_file tst rcsinfo
run "(echo ''; echo 'ALL \$CVSROOT/CVSROOT/shiela.msg'; echo '') >>rcsinfo"
cvs_file add rcsinfo

cvs_file tst taginfo
run "(echo ''; echo 'ALL $V_tool_shiela --hook=taginfo %t %o %p %{sv}'; echo '') >>taginfo"
cvs_file add taginfo

if [ ".$CVS_VENDOR" = .RSE ]; then
    cvs_file tst importinfo
    run "(echo ''; echo 'ALL $V_tool_shiela --hook=importinfo'; echo '') >>importinfo"
    cvs_file add importinfo

    cvs_file tst admininfo
    run "(echo ''; echo 'ALL $V_tool_shiela --hook=admininfo'; echo '') >>admininfo"
    cvs_file add admininfo
fi

cvs_file tst commitinfo
run "(echo ''; echo 'ALL $V_tool_shiela --hook=commitinfo %r/%p %s'; echo '') >>commitinfo"
cvs_file add commitinfo

cvs_file tst verifymsg
run "(echo ''; echo 'DEFAULT $V_tool_shiela --hook=verifymsg %l'; echo '') >>verifymsg"
cvs_file add verifymsg

if [ ".$CVS_VENDOR" = .RSE ]; then
    flags="sVvto"
else
    flags="sVv"
fi
cvs_file tst loginfo
run "(echo ''; echo 'ALL $V_tool_shiela --hook=loginfo %p %{${flags}}'; echo '') >>loginfo"
cvs_file add loginfo

run $V_tool_cvs update

echo ""
echo "${term_bold}The above modifications were made to CVSROOT.${term_norm}"
text="${term_bold}Are you sure to finally commit these modifications [y/N]: ${term_norm}"
echo dummy | awk '{ printf("%s", TEXT); }' TEXT="$text"
if [ ".$V_batch" = .yes ]; then
    echo ""
else
    read answer
    if [ ".$answer" = . -o ".$answer" = .n ]; then
        cd ..
        rm -rf CVSROOT
        echo ""
        echo "**Break: nothing touched."
        echo ""
        exit 0
    fi
fi
echo ""
run "$V_tool_cvs commit -m 'Add OSSP shiela -- Access Control and Logging Facility for CVS' ."
run cd ..
run rm -rf CVSROOT

cat <<EOT

Ok, your CVS repository was modified to now use OSSP shiela for access
control and logging. You can try it out by checking out CVSROOT
yourself and modify some files. You should receive an Email for every
modification. Then especially adjust "shiela.cfg" for your particular
repository layout.

EOT

rm -f $tmpfile
rm -f $tmpdir/shiela.cfg
rm -f $tmpdir/shiela.msg

