   _        ___  ____ ____  ____        _     _      _
  |_|_ _   / _ \/ ___/ ___||  _ \   ___| |__ (_) ___| | __ _
  _|_||_| | | | \___ \___ \| |_) | / __| '_ \| |/ _ \ |/ _` |
 |_||_|_| | |_| |___) |__) |  __/  \__ \ | | | |  __/ | (_| |
  |_|_|_|  \___/|____/____/|_|     |___/_| |_|_|\___|_|\__,_|

  OSSP shiela - Access Control and Logging Facility for CVS
  ____________________________________________________________________

  ChangeLog

  Changes between 1.1.7 and 1.1.8 (25-Jul-2006 to xx-xxx-200x):

  Changes between 1.1.6 and 1.1.7 (03-Oct-2005 to 25-Jul-2006):

   *) Fix "arbitrary shell command execution" security bug caused by
      missing shell command argument escaping for user supplied arguments.
      [Brian Caswell <bmc@shmoo.com>, Ralf S. Engelschall] (CVE-2006-3633)

   *) Upgraded build environment to GNU shtool 2.0.6 and
      GNU autoconf 2.60
      [Ralf S. Engelschall]

   *) Adjust copyright messages for new year 2006.
      [Ralf S. Engelschall]

  Changes between 1.1.5 and 1.1.6 (12-Jan-2005 to 03-Oct-2005):
   
   *) Upgraded build environment to GNU shtool 2.0.3
      [Ralf S. Engelschall]

  Changes between 1.1.4 and 1.1.5 (02-Jul-2004 to 12-Jan-2005):

   *) Upgraded build environment to GNU shtool 2.0.1
      [Ralf S. Engelschall]

   *) Fix internal error handling by replacing "|| die" with "or die"
      constructs because the different binding priority of "||" and "or"
      leaded to wrong results.
      [Ralf S. Engelschall, Geoff Thorpe <geoff@geoffthorpe.net>]

   *) Fix shiela-install.pod's description of --loginfo hook for the
      non-RSE-patches situation: the %p construct has to be used
      there, too.
      [Geoff Thorpe <geoff@geoffthorpe.net>]

   *) Adjust copyright messages for new year 2005.
      [Ralf S. Engelschall]

   *) Workaround a buggy feature in Perl versions 5.8.4 and higher
      which totally optimized away "my $var = undef;" constructs
      instead of optimizing them to just "my $var;".
      [Ralf S. Engelschall, Thomas Lotterer]

  Changes between 1.1.3 and 1.1.4 (27-Jun-2004 to 02-Jul-2004):

   *) Fix determination of the "handle" field under new CVS 1.12.x
      where ISO format dates are used in the output of "cvs log".
      [Ralf S. Engelschall]

   *) Document in install-shiela.pod and install-shiela.sh that
      CVS version 1.12.6 or higher is required.
      [Ralf S. Engelschall]

  Changes between 1.1.2 and 1.1.3 (10-May-2004 to 27-Jun-2004):

   *) Fix determination of the line counts in the "Changes" field
      by no longer announcing to the CVS server that OSSP shiela can
      handle "MT" (message tagged) responses. The problem is CVS 1.12.x
      starts to send some important responses as "MT" responses now and
      OSSP shiela only accepts "M" responses.
      [Ralf S. Engelschall, Geoff Thorpe <geoff@geoffthorpe.net>]

  Changes between 1.1.1 and 1.1.2 (10-May-2002 to 10-May-2004):

   *) Optimize and bugfix the determination of the number of
      lines in case of added files.
      [Ralf S. Engelschall]

   *) Fixed two more warnings about undefined variables.
      [Ralf S. Engelschall, Michael Schloh von Bennewitz]

   *) Fixed typos in source comments.
      [Michael Schloh von Bennewitz]

  Changes between 1.1.0 and 1.1.1 (07-May-2002 to 10-May-2004):

   *) Workaround a syntax problem under Solaris /bin/sh.
      [Ralf S. Engelschall]

  Changes between 1.0.4 and 1.1.0 (23-Dec-2002 to 07-May-2004):

   *) Add "shiela-test.sh" program and "make test" driver
      for a minimal local test suite.
      [Ralf S. Engelschall]

   *) Upgraded to the new CVS 1.12.x info format strings.
      OSSP shiela now works with CVS >= 1.12.x only.
      [Ralf S. Engelschall]

   *) Fixed shiela-install: "diff:mime" -> "patch:plain".
      [Ralf S. Engelschall]

   *) Bump year in copyright messages for 2003 and 2004.
      [Ralf S. Engelschall]

   *) Upgrade build environment to GNU autoconf 2.59
      [Ralf S. Engelschall]

   *) Provide CVS 1.12.x "cvs rdiff" support in detail outputs.
      [Ralf S. Engelschall]

   *) Support CVS 1.12.x (option -l no longer existing).
      [Ralf S. Engelschall]

   *) Fix a nasty syntax error in shiela-install.sh occurring
      if variables are passes as command line parameters.
      [Ralf S. Engelschall]

   *) Allow Shiela to accept CVS 1.12 and higher, too.
      [Ralf S. Engelschall]

  Changes between 1.0.3 and 1.0.4 (23-Dec-2002 to 23-Dec-2002):

   *) Log also the user id of the committer in the OSSP shiela logfile
      to remove the burden on foreign applications having to merge
      the CVSROOT/history and the shiela logfile in order to get all
      information. Additionally the seperator character in the OSSP
      shiela logfile was changed from a comma (",") to a bar ("|")
      character in order to be more similar to CVSROOT/history.
      [Ralf S. Engelschall]

   *) Added "Setenv <variable <value>" configuration command to
      "Environment" configuration section which allows one to set
      environment variables like PATH, etc. This especially allows now
      non-absolute paths on "Program" configuration commands.
      [Ralf S. Engelschall]

   *) Fixed indentation on "Content files" report part.
      [Ralf S. Engelschall]

  Changes between 1.0.2 and 1.0.3 (23-Dec-2002 to 23-Dec-2002):

   *) CVS since 1.11.2 allows verifymsg-hooked scripts to actually
      change the message and reads the contents back. So we do no longer
      require CVS with RSE patches applied for the verifymsg hook to be
      activated.
      [Ralf S. Engelschall]

   *) Added a section to the shiela-install manual page about the
      internal processing steps performed by CVS. Additionally mention
      the details about the CVS version with RSE patches applied.
      [Ralf S. Engelschall]

   *) Make sure that the header in reports is not optically destroyed by
      too long columns (as it is the case all the time for PMOD commits
      in the OpenPKG project).
      [Ralf S. Engelschall]

   *) Correctly recognize and configure RSE CVS version in
      shiela-install program.
      [Ralf S. Engelschall]

   *) Be smart and allow a RSE CVS version to be driven like a stock
      CVS version in loginfo (still using "sVv" instead of "sVvto" as
      the flags).
      [Ralf S. Engelschall]

  Changes between 1.0.1 and 1.0.2 (22-Dec-2002 to 23-Dec-2002):

   *) Make sure that /bin:/usr/bin:/sbin:/usr/sbin is in $PATH
      when locating tool.
      [Ralf S. Engelschall]

   *) Correctly use the path in "Program uuencode <path>" when
      running uuencode.
      [Ralf S. Engelschall]

   *) Fixed typos in manual pages.
      [Ralf S. Engelschall]

   *) Avoid over-sized lines in xdelta based patch scripts.
      [Ralf S. Engelschall]

  Changes between 1.0.0 and 1.0.1 (22-Dec-2002 to 22-Dec-2002):

   *) Fixed run-time under Perl 5.8.0: import only abs_path()
      from module Cwd to avoid conflicts with POSIX module.
      [Ralf S. Engelschall]

  Changes between 0.9.2 and 1.0.0 (19-Aug-2002 to 22-Dec-2002):

   *) Add branch information to the Subject lines of generated Emails.
      Additionally, for better readability, use a trailing slash on
      directory names in the Subject lines of generated Emails.
      [Ralf S. Engelschall]

   *) Line-break single-line log messages into multi-line log messages
      to make the usual log messages produced by "cvs commit -m '...'"
      more readable in the report.
      [Ralf S. Engelschall]

   *) Fully reimplemented the "Content" "details" (the change summary in
      reports). It now supports both textual (via diff(1)) and binary
      (via xdelta(1)) file changes. Additionally each change report part
      is now a small executable shell-script which can passed through
      /bin/sh for reproducing the patch locally. This way, especially
      binary change reports are more meaningsful and consistent with the
      textual change reports.
      INCOMPATIBILITY:
      "Details diff:plain" -> "Details patch:plain"
      "Details diff:mime" -> "Details patch:mime"
      [Ralf S. Engelschall]

   *) Make sure that CVS diff handles are calculated correctly
      if files are added or deleted.
      [Ralf S. Engelschall]

   *) Fix incorrect use (and hence producing a warning for uninitialized
      variable) of variables in the writing of history files.
      [Ralf S. Engelschall]

   *) Finally really use the "Environment" configuration section to find
      "cvs" and "sendmail" directly and add a "Program" sub-command to it
      for easier extension of the "Environment" section in the future.
      INCOMPATIBILITY:
      "CVS <path>" -> "Program cvs <path>"
      "Sendmail <path>" -> "Program sendmail <path>"
      [Ralf S. Engelschall]

   *) Consistently use IO objects instead of the anchient direct
      fiddling with Perl's filedescriptor symbol globs.
      [Ralf S. Engelschall]

   *) Correctly determine CVS version and optional RSE patches (from
      OpenPKG "cvs" package).
      [Ralf S. Engelschall]

   *) Upgraded to GNU shtool 1.6.2 and GNU autoconf 2.57.
      [Ralf S. Engelschall]

   *) Fix the "invalid file specification `<absolute path>' for access
      control" issue by using Cwd::realpath to resolve the absolute path
      instead of calling `pwd`.
      [Ralf S. Engelschall]

   *) Consistently switch to the "OSSP shiela" branding.
      [Ralf S. Engelschall]

  Changes between 0.9.1 and 0.9.2 (10-Feb-2001 to 19-Aug-2002):

   *) Switched to the OSSP devtool build environment
      and upgraded to GNU shtool 1.6.1 and GNU autoconf 2.53.
      [Ralf S. Engelschall]

   *) Fixed warning in dereferencing uninitialized variable.
      [Ralf S. Engelschall, Markus Sander]

   *) Fixed information gathering for stock CVS version.
      [Ralf S. Engelschall]

   *) Fixed +d/-d output on removed files.
      [Ralf S. Engelschall]

   *) Fixed meta character handling in regex matchings.
      [Ralf S. Engelschall]

   *) Make sure shiela accepts CVS 1.11 and newer, too.
      [Ralf S. Engelschall]

   *) Adjusted shiela-install's Perl tool checks to use
      the logic of GNU shtool' "path" command.
      [Ralf S. Engelschall]

   *) Upgraded to GNU shtool 1.5.3
      [Ralf S. Engelschall]

  Changes between 0.9.0 and 0.9.1 (18-Jun-2000 to 10-Feb-2001):

   *) Upgraded to GNU shtool 1.5.2-pre.
      [Ralf S. Engelschall]

   *) Added --with-perl=PATH and --with-cvs=PATH Autoconf options
      to allow one to force the use of particular programs.
      [Ralf S. Engelschall]

   *) Added $(DESTDIR) support for "make install".
      [Ralf S. Engelschall]

  Changes between *GENESIS* and 0.9.0 (Apr-2000 to 18-Jun-2000):

   *) Created the first OSSP shiela version.
      [Ralf S. Engelschall]

